jQuery(function($) { 
	$(document).ready(function() { 

		// updated fix for placeholders in IE 8 & 9
		// from https://gist.github.com/pjeweb/1320822
		// fork of https://gist.github.com/hagenburger/379601
		
		$('[placeholder]')
	        .focus(function () {
	            var input = $(this);
	            if (input.val() === input.attr('placeholder')) {
	                input.val('').removeClass('placeholder');
	            }
	        })
	        .blur(function () {
	            var input = $(this);
	            if (input.val() === '' || input.val() === input.attr('placeholder')) {
	                input.addClass('placeholder').val(input.attr('placeholder'));
	            }
	        })
	        .blur()
	        .parents('form').submit(function () {
	            $(this).find('[placeholder]').each(function () {
	                var input = $(this);
	                if (input.val() === input.attr('placeholder')) {
	                    input.val('');
	                }
	            });
	        });		
		
		// end fix for form placeholders in IE 8 & 9
	
	
	}); 
}); 
