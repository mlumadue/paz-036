	<div id="page-wrapper">

		<header id="header" class="section clearfix">
			<section class="col-xs-12 col-sm-12 col-md-7">
			  <?php if ($logo): ?>
					<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
						<img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>">
					</a>
			  <?php endif; ?>
			</section>
			<?php if ($page['header']): ?>
				<section id="hero" class="col-xs-12">
					<?php print render($page['header']); ?>
				</section>
			<?php endif ?>
		</header>

		<?php if ($page['nav']): ?>
			<nav id="navigation" class="clearfix">
			<?php print render($page['nav']); ?>
		</nav> <!-- /.section, /#navigation -->
		<?php endif; ?>
    
		<?php if ($breadcrumb): ?>
			<div id="breadcrumb" class="clearfix"><?php print $breadcrumb; ?></div>
		<?php endif; ?>
		
		<?php if ($messages): ?>
			<div id="messages" class="clearfix"><?php print $messages; ?></div>
		<?php endif; ?>

		<div id="main-wrapper" class="clearfix">
		
			<section id="highlighted" class="clearfix">
				<div id="highlight">
					<?php print render($page['highlighted']); ?>
				</div>
			</section>
		
			<div id="main" class="clearfix">
				<section id="content" class="col-xs-12 <?php if ($page['right'] && $page['left']):print 'col-sm-8 col-md-6 col-md-push-3';elseif ($page['left'] && !$page['right']):print 'col-sm-7 col-sm-push-5 col-md-8 col-push-md-4';elseif ($page['right'] && !$page['left']):print 'col-sm-12 col-md-7';else:print 'col-sm-12 clearfix';endif; ?>">
					<a id="main-content"></a>
					
					<header>
						<?php print render($title_prefix); ?>
						<?php if ($title): ?><h1 class="title" id="page-title"><?php print $title; ?></h1><?php endif; ?>
						<?php print render($title_suffix); ?>
						<?php if ($tabs): ?><div class="tabs"><?php print render($tabs); ?></div><?php endif; ?>
						<?php print render($page['help']); ?>
						<?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
					</header>
					<?php print render($page['content']); ?>
					<footer>
						<?php print $feed_icons; ?>
					</footer>
				</section> <!-- /.section, /#content -->

				<?php if ($page['left']): ?>
					<aside id="left" class="col-xs-12 <?php if ($page['right']): print "col-sm-3 col-md-3 col-md-pull-6";else: print "col-sm-5 col-sm-pull-7 col-md-4 col-pull-md-8";endif; ?> sidebar">
						<?php print render($page['left']); ?>
					</aside> <!-- /#right -->
				<?php endif; ?>

				<?php if ($page['right']): ?>
					<aside id="right" class="col-xs-12 <?php if ($page['left']): print "col-sm-12 col-md-3";else: print "col-sm-4 col-md-4";endif; ?> sidebar">
						<?php print render($page['right']); ?>
					</aside> <!-- /#right -->
				<?php endif; ?>
			</div><!-- /#main -->
	  
			<?php if ($page['secondary']): ?>
				<aside id="secondary" class="clearfix">
					<?php print render($page['secondary']); ?>
				</aside> <!-- /.section, /#secondary -->
			<?php endif; ?>

			<?php if ($page['tertiary']): ?>
				<aside id="tertiary" class="clearfix">
					<?php print render($page['tertiary']); ?>
				</aside> <!-- /.section, /#secondary -->
			<?php endif; ?>
		</div> <!-- /#main-wrapper -->

		<?php if ($page['footer']): ?>
			<footer id="footer" class="clearfix">
				<div id="footer-footer" class="section">
					<?php print render($page['footer']); ?>
				</div>
			</footer>
		<?php endif ?>

	</div> <!-- /#page, /#page-wrapper -->
